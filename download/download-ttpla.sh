#!/bin/bash

OUT="$1/ttpla"
ID="1Yz59yXCiPKS0_X4K3x9mW22NLnxjvrr0"

printusage(){ 
  echo "Usage: $0 <output-directory>" 
}

error(){
  cleanup
  exit 2
}

cleanup(){
  rm -rf "$OUT/data_original_size_v1"
  rm -rf "$OUT/data.zip"
}

[ -z $1 ] && printusage && exit 1

./gdrive-download.sh $ID $OUT || error

# Using 7z because the archive is broken and unzip can't read it
7z x "$OUT/data.zip" -o"$OUT"
mv "$OUT/data_original_size_v1/data_original_size"/* "$OUT"

cleanup
