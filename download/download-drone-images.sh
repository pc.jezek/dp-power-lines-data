#!/bin/bash

SCRIPT_DIR="$(dirname "$0")"
VARIANT=$2

OUT="$1/images"

printusage(){ 
  echo "Usage: $0 <output-directory> <variant 1-3>" 
}

error(){
  cleanup
  exit 2
}

cleanup(){
  rm -rf "$OUT/data.zip"
  rm -rf "$OUT/mission-1"
}


if ! [[ "$VARIANT" =~ ^[1-3]$ ]]; then
  printusage
  exit 3
fi

[[ $VARIANT == "1" ]] && ID="1vzWVmZECUn0vdXr1XeqH-A09wwxRnNfL"
[[ $VARIANT == "2" ]] && ID="17mHYUgh4cNEjkUsUvi9cML_ysKczt2YZ"
[[ $VARIANT == "3" ]] && ID="1A41G9mQe9Po3D419TMWy1vGD1kd0PGoU"

[ -z $1 ] && printusage && exit 1

"$SCRIPT_DIR/gdrive-download.sh" $ID $OUT || error

unzip "$OUT/data.zip" -d "$OUT"
mv "$OUT"/mission-1/* "$OUT"

cleanup
